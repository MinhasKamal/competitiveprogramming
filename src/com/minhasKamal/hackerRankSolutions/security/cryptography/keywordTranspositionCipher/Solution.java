package com.minhasKamal.hackerRankSolutions.security.cryptography.keywordTranspositionCipher;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
		
		int TestCases = scanner.nextInt();
        scanner.nextLine();
		for(int Case=1; Case<=TestCases; Case++){
			
            String keyWord = removeDuplicateChar(scanner.nextLine());
            int[] encryptionSubstitution = generateSubstitution(keyWord);
            int[] decryptionSubstitution = encrToDecrSubstitution(encryptionSubstitution);
            
            /*for(int i=0; i<alph.length; i++){
                System.out.print((char) alph[i]);
            }System.out.println();*/
            
            String ciphertext = scanner.nextLine();
            
            int size = ciphertext.length();
            for(int i=0; i<size; i++){
                char ch = ciphertext.charAt(i);
                
                if(ch == ' '){
                    System.out.print(" ");
                }else{
                    System.out.print((char) decryptionSubstitution[ch-65]);
                }
            }
            System.out.println();
		}
		scanner.close();
    }
    
    static int[] encrToDecrSubstitution(int[] encryptionSubstitution){
        int[] decryptionSubstitution = new int[encryptionSubstitution.length];
        
        for(int i=0; i<encryptionSubstitution.length; i++){
            decryptionSubstitution[encryptionSubstitution[i]-65] = 65+i;
        }
        
        return decryptionSubstitution;
    }
    
    static int[] generateSubstitution(String keyWord){
       
        int[] preSubstitutionAlphs = generatePreSubstitution(keyWord);
        
        int[] substitutionAlphs = new int[26];
        int size = keyWord.length();
        int[] sequance = getSequance(preSubstitutionAlphs, size);
        for(int i=0, index=0; i<size; i++){
            int level=0;
            
            while(level+sequance[i]<26){
                substitutionAlphs[index] = preSubstitutionAlphs[level+sequance[i]];
                //System.out.println(index+" "+substitutionAlphs[index]);
                level += size;
                index++;
            }
        }
        
        return substitutionAlphs;
    }
    
    static int[] getSequance(int[] substitutionAlphs, int size){
        int[] sequance = new int[size];
        int[] alphs = new int[size];
        for(int i=0; i<size; i++){
            sequance[i] = i;
            alphs[i] = substitutionAlphs[i];
        }
        
        int temp;
        for(int i=0; i<size; i++){
            for(int j=i+1; j<size; j++){
                if(alphs[i]>alphs[j]){
                    temp = alphs[i];
                    alphs[i] = alphs[j];
                    alphs[j] = temp;
                    
                    temp = sequance[i];
                    sequance[i] = sequance[j];
                    sequance[j] = temp;
                }
            }
        }
        
        /*for(int i=0; i<size; i++){
            System.out.println(alphs[i]+" "+sequance[i]);
        }*/
        
        return sequance;
    }
    
    static int[] generatePreSubstitution(String keyWord){
        int keyWordSize = keyWord.length();
        int[] initAlphs = new int[26];
        for(int i=0; i<keyWordSize; i++){
            initAlphs[i] = keyWord.charAt(i);
        }
        
        for(int i=keyWordSize, ch=0; i<26; ch++){
            if(!keyWord.contains((char)(65+ch) + "")){
                initAlphs[i] = ch+65;
                i++;
            }
        }
        
        return initAlphs;
    }
    
    static String removeDuplicateChar(String string){
        String newString = "";
        int size = string.length();
        for(int i=0; i<size; i++){
            char ch = string.charAt(i);
            if(!newString.contains(ch+"")){
                newString += ch;
            }
        }
        return newString;
    }
}