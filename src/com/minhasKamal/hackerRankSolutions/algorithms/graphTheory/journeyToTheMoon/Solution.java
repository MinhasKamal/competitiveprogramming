package com.minhasKamal.hackerRankSolutions.algorithms.graphTheory.journeyToTheMoon;

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
        String[] temp = bfr.readLine().split(" ");
        int N = Integer.parseInt(temp[0]);
        int I = Integer.parseInt(temp[1]);
      
        int[] group = new int[N];
        for(int i=0; i<N; i++){
            group[i] = -1;
        }
        
        int groupLabel=0;
        for(int i = 0; i < I; i++){
            temp = bfr.readLine().split(" ");
            int a = Integer.parseInt(temp[0]);
            int b = Integer.parseInt(temp[1]);
            // Store a and b in an appropriate data structure of your choice
            
            if(group[a]<0 && group[b]<0){
                group[a] = groupLabel;
                group[b] = groupLabel;
                groupLabel++;
            }else if(group[a]<0){
                group[a] = group[b];
            }else if(group[b]<0){
                group[b] = group[a];
            }else{
                int grpPrevLabel = group[a];
                int grpLateLabel = group[b];
                
                if(grpPrevLabel > grpLateLabel){
                    int tempLbl = grpLateLabel;
                    grpLateLabel = grpPrevLabel;
                    grpPrevLabel = tempLbl;
                }
                
                for(int j=0; j<N; j++){
                    if(group[j] == grpLateLabel){
                        group[j] = grpPrevLabel;
                    }
                }
            }
        }

        // Compute the final answer - the number of combinations
        if(groupLabel==1){
            boolean flag=true;
            for(int j=0; j<N; j++){
                if(group[j]!=1){
                    flag=false;
                    break;
                }
            }
            if(flag){
                System.out.println(0);
                return;
            }
        }
        
        long[] groupSize = new long[groupLabel+1];
        for(int j=0; j<N; j++){
            if(group[j]<0){
                groupSize[groupLabel]++;
            }else{
                groupSize[group[j]]++;
            }
        }
       
        long combinations = ( groupSize[groupLabel] * (N-groupSize[groupLabel]) ) +
            ( ( groupSize[groupLabel] * (groupSize[groupLabel]-1) ) / 2 );
        
        for(int j=0; j<groupLabel; j++){
            for(int k=j+1; k<groupLabel; k++){
                combinations += (groupSize[j]*groupSize[k]);
            }
        }
        
        System.out.println(combinations);

    }
}
