package com.minhasKamal.hackerRankSolutions.algorithms.implementation.matrixLayerRotation;

import java.util.*;

public class Solution {

	static int[][] rotate(int[][] matrix, int row, int col, int rotation){
		
		Node[] nodes = matrixToLinkedList(matrix, row, col);
		nodes = rotate(nodes, rotation);
		//printNodes(nodes);
		matrix = linkedListToMatrix(nodes, row, col);
		
		return matrix;
	}
	
	static Node[] matrixToLinkedList(int[][] matrix, int row, int col){
		int rowHalf = (row+1)/2;
		
		Node[] nodes = new Node[rowHalf];
		for(int i=0; i<rowHalf; i++){
			nodes[i] = new Node(-1);
		}
		
		for(int i=0; i<rowHalf; i++){
			Node list = nodes[i];
			
			for(int j=i; j<=col-1-i; j++){ //up
				//System.out.print(matrix[i][j]+" ");
				Node node = new Node(matrix[i][j]);
				list.next = node;
				list = list.next;
			}
			
			if(col-1-i < i){
				continue;
			}
			for(int j=i+1; j<=row-2-i; j++){ //right
				//System.out.print(matrix[j][col-1-i]+" ");
				Node node = new Node(matrix[j][col-1-i]);
				list.next = node;
				list = list.next;
			}
			
			if(i==row-1-i){
				continue;
			}
			for(int j=col-1-i; j>=i; j--){ //down
				//System.out.print(matrix[row-i-1][j]+" ");
				Node node = new Node(matrix[row-i-1][j]);
				list.next = node;
				list = list.next;
			}
			
			if(i==col-1-i){
				continue;
			}
			for(int j=row-2-i; j>=i+1; j--){ //left
				//System.out.print(matrix[j][i]+" ");
				Node node = new Node(matrix[j][i]);
				list.next = node;
				list = list.next;
			}
		}
		
		return nodes;
	}
	
	static int[][] linkedListToMatrix(Node[] nodes, int row, int col){
		int rowHalf = (row+1)/2;
		
		int[][] matrix = new int[row][col];		
		
		for(int i=0; i<rowHalf; i++){
			Node list = nodes[i];
			
			for(int j=i; j<=col-1-i; j++){ //up
				//System.out.print(matrix[i][j]+" ");
				matrix[i][j] = list.next.value;
				list = list.next;
			}
			
			if(col-1-i < i){
				continue;
			}
			for(int j=i+1; j<=row-2-i; j++){ //right
				//System.out.print(matrix[j][col-1-i]+" ");
				matrix[j][col-1-i] = list.next.value;
				list = list.next;
			}
			
			if(i==row-1-i){
				continue;
			}
			for(int j=col-1-i; j>=i; j--){ //down
				//System.out.print(matrix[row-i-1][j]+" ");
				matrix[row-i-1][j] = list.next.value;
				list = list.next;
			}
			
			if(i==col-1-i){
				continue;
			}
			for(int j=row-2-i; j>=i+1; j--){ //left
				//System.out.print(matrix[j][i]+" ");
				matrix[j][i] = list.next.value;
				list = list.next;
			}
		}
		
		return matrix;
	}
	
	static Node[] rotate(Node[] nodes, int rotation){
		
		for(int i=0; i<nodes.length; i++){
			int length = calcLength(nodes[i].next);
			if(length==0){
				continue;
			}
			
			int actualRotation = rotation%length;
			if(actualRotation==0){
				continue;
			}
			
			rotateNode(nodes[i], actualRotation);
		}
		
		return nodes;
	}
	
	static void rotateNode(Node head, int actualRotation){
		Node targetNode = head;
		while(actualRotation-- > 0){
			targetNode = targetNode.next;
		}
		
		Node tempNode = head.next;
		head.next = targetNode.next;
		targetNode.next = null;
		
		targetNode = head;
		while(targetNode.next != null){
			targetNode = targetNode.next;
		}
		targetNode.next = tempNode;
	}
	
	static int calcLength(Node node){
		int length=0;
		while(node != null){
			length++;
			node = node.next;
		}
		return length;
	}
	
	static void printNodes(Node[] nodes){
		for(int i=0; i<nodes.length; i++){
			Node list = nodes[i].next;
			while(list != null){
				System.out.print(list.value+" ");
				list = list.next;
			}
			System.out.println();
		}
	}
	
    public static void main(String[] args) throws Exception {
    	/*/System.setIn(new java.io.FileInputStream(Solution.class.getResource("input.txt").getPath()));/**/
        Scanner in = new Scanner(System.in);
        
        int row = in.nextInt();
        int col = in.nextInt();
        int rotation = in.nextInt();
        
        int[][] matrix = new int[row][col];
        for(int i=0; i<row; i++){
        	for(int j=0; j<col; j++){
        		matrix[i][j] = in.nextInt();
        	}
        }
        in.close();
        
        matrix = rotate(matrix, row, col, rotation);
        
        for(int i=0; i<row; i++){
        	for(int j=0; j<col; j++){
        		System.out.print(matrix[i][j]+" ");
        	}
        	System.out.println();
        }
    }

}

class Node{
	int value;
	Node next;
	
	Node(int val){
		value = val;
		next = null;
	}
}