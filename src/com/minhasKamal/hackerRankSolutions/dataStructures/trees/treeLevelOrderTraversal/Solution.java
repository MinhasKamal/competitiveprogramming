package com.minhasKamal.hackerRankSolutions.dataStructures.trees.treeLevelOrderTraversal;

import java.util.LinkedList;

public class Solution{
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	void LevelOrder(Node root){
        if(root == null){
            return;
        }
        
        
        LinkedList queue = new LinkedList<Node>();
        queue.add(root);
        while(!queue.isEmpty()){
            root = (Node) queue.poll();
            System.out.print(root.data+" ");
            if(root.left != null){
                queue.add(root.left);
            }if(root.right != null){
                queue.add(root.right);
            }
        }
        
        return;
    }
	
	class Node {
		int data;
		Node left;
		Node right;
	}
}