package com.minhasKamal.hackerRankSolutions.dataStructures.trees.treeHuffmanDecoding;

public class Solution{
	
	void decode(String S, Node root){
	    int length = S.length();
	    Node list = root;
	    char ch;
	    
	    for(int i=0; i<length; i++){
	        ch = S.charAt(i);
	        
	        if(list.left == null){
	            System.out.print(list.data);
	            list = root;
	        }
	        
	        if(ch == '0'){
	            list = list.left;
	        }else{
	            list = list.right;
	        }
	    }
	    System.out.print(list.data);
	    
	    return;
	}
	
	class Node {
		int data;
		Node left;
		Node right;
	}
}