package com.minhasKamal.hackerRankSolutions.dataStructures.trees.isThisABinarySearchTree;

public class Solution{
	
    int value;
    
    boolean checkBST(Node root) {
        value = -999;
        
        return checkBinaryTree(root);
    }

    boolean checkBinaryTree(Node root){
        boolean isBinaryTree = true;
        
        if(root != null){
            isBinaryTree = isBinaryTree && checkBinaryTree(root.left);
            
            if(value >= root.data || !isBinaryTree){
                return false;
            }
            value = root.data;
            
            isBinaryTree = isBinaryTree && checkBinaryTree(root.right);
        }
        
        return isBinaryTree;
    }
	
	class Node {
		int data;
		Node left;
		Node right;
	}
}