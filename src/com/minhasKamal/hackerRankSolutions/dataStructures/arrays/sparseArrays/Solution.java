package com.minhasKamal.hackerRankSolutions.dataStructures.arrays.sparseArrays;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        /// input
        Scanner scanner = new Scanner(System.in);
        
        int arraySize = scanner.nextInt();
        
        String[] array = new String[arraySize];
        for(int i=0; i<arraySize; i++){
            array[i] = scanner.next();
        }
        
        int queries = scanner.nextInt();
        String queryString;
        for(int i=0; i<queries; i++){
            queryString = scanner.next();
            
            /// process
            int occurance=0;
            
            for(int j=0; j<arraySize; j++){
                if(queryString.equals(array[j])){
                    occurance++;
                }
            }
            
            /// output
            System.out.println(occurance);
        }
        
        scanner.close();
    }
}