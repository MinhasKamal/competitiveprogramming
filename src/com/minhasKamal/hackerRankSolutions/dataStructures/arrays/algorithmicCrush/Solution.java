package com.minhasKamal.hackerRankSolutions.dataStructures.arrays.algorithmicCrush;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        /// input
        Scanner scanner = new Scanner(System.in);
        
        int arraySize = scanner.nextInt();
        long[] array = new long[arraySize+1];
        for(int i=0; i<=arraySize; i++){
            array[i] = 0;
        }
        
        int operations = scanner.nextInt();
        for(int i=0; i<operations; i++){
            int start = scanner.nextInt();
            int end = scanner.nextInt();
            long value = scanner.nextLong();
            
            array[start-1] += value;
            array[end] -= value;
        }
        
        
        /// process
        long sum = array[0];
        long maxValue = array[0];
        for(int i=1; i<arraySize; i++){
            sum += array[i];
            if(maxValue<sum){
                maxValue=sum;
            }
        }
        
       
        /// output
        System.out.print(maxValue);
        scanner.close();
    }
}