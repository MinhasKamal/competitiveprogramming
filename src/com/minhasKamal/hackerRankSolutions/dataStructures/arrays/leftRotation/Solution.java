package com.minhasKamal.hackerRankSolutions.dataStructures.arrays.leftRotation;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        /// input
        Scanner scanner = new Scanner(System.in);
        
        int arraySize = scanner.nextInt();
        int rotation = scanner.nextInt();
        
        int[] array = new int[arraySize];
        for(int i=0; i<arraySize; i++){
            array[i] = scanner.nextInt();
        }
        
        /// process
        rotation = rotation%arraySize;
        
        int[] rotatedArray = new int[arraySize];
        for(int i=0; i<arraySize; i++){
            rotatedArray[i] = array[rotation];
            rotation++;
            if(rotation==arraySize){
                rotation=0;
            }
        }
        
        /// output
        for(int i=0; i<arraySize; i++){
            System.out.print(rotatedArray[i]+" ");
        }
        
        scanner.close();
    }
}