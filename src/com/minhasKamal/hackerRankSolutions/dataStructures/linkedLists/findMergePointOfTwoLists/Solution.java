package com.minhasKamal.hackerRankSolutions.dataStructures.linkedLists.findMergePointOfTwoLists;

public class Solution{
	
	int FindMergeNode(Node headA, Node headB) {
	    // Complete this function
	    // Do not write the main method. 
	    
	    while(headA != null){
	        
	        Node listB = headB;
	        while(listB != null){
	            if(headA == listB){
	                return headA.data;
	            }
	            listB = listB.next;
	        }
	        
	        headA = headA.next;
	        
	    }
	    
	    return 0;
	}
	
	class Node {
		int data;
		Node next;
	}

}