package com.minhasKamal.hackerRankSolutions.dataStructures.linkedLists.cycleDetection;

public class Solution{
	
	boolean hasCycle(Node head) {
	    
	    if(head==null){
	        return false;
	    }
	    
	    Node slowList = head, fastList = head;
	    while(fastList != null && fastList.next != null){
	        slowList = slowList.next;
	        fastList = fastList.next.next;
	            
	        if(slowList == fastList){
	            return true;
	        }
	    }
	    
	    return false;
	}
	
	class Node {
		int data;
		Node next;
	}
}