/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.jumpingMario_11764;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	static int[] WallHeights;
	
	static int HighJumps, LowJumps;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		WallHeights = new int[N];
		
		for(int i=0; i<N; i++){
			WallHeights[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		HighJumps=LowJumps=0;
		
		for(int i=1; i<N; i++){
			if(WallHeights[i-1] < WallHeights[i]){
				HighJumps++;
			}else if(WallHeights[i-1] > WallHeights[i]){
				LowJumps++;
			}
		}
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+HighJumps+" "+LowJumps);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/