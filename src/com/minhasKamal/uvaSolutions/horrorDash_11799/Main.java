/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.horrorDash_11799;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
	}
	
	static void process(){
		Answer = scanner.nextInt();
		for(int i=1; i<N; i++){
			int temp = scanner.nextInt();
			if(Answer<temp){
				Answer=temp;
			}
		}
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/