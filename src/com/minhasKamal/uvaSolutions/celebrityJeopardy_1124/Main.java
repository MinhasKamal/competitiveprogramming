/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.celebrityJeopardy_1124;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String IN;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		IN = scanner.nextLine();
	}
	
	static void process(){
		Answer=IN;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/