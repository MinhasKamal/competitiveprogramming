/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.listOfConquests_10420;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N;
	static String[] Countries;
	
	static int[] Answer;
	static int index;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		Countries = new String[N];
		Answer = new int[N];
	}
	
	static void process(){
		Countries[0] = scanner.next();
		index=1;
		
		scanner.nextLine();
		for(int i=1; i<N; i++){
			String country = scanner.next();
			scanner.nextLine();
			
			for(int j=0; j<index; j++){
				if(Countries[j].equals(country)){
					Answer[j]++;
					country="";
					break;
				}
			}
			
			if(country.length()>0){
				Countries[index] = country;
				index++;
			}
		}
		
		sort();
	}
	
	static void sort(){
		for(int i=0; i<index; i++){
			for(int j=i+1; j<index; j++){
				if(compare(Countries[i], Countries[j])){
					String strTemp = Countries[i];
					Countries[i] = Countries[j];
					Countries[j] = strTemp;
					
					int intTemp = Answer[i];
					Answer[i] = Answer[j];
					Answer[j] = intTemp;
				}
			}
		}
	}
	
	static boolean compare(String str1, String str2){
		for(int i=0; i<str1.length(); i++){
			if(i>=str2.length()){
				return true;
			}
			if(str1.charAt(i)>str2.charAt(i)){
				return true;
			}if(str1.charAt(i)<str2.charAt(i)){
				return false;
			}
		}
		
		return false;
	}
	
	static void output(){
		for(int i=0; i<index; i++){
			System.out.println(Countries[i] + " " + (Answer[i]+1));
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		input();
		process();
		output();
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/