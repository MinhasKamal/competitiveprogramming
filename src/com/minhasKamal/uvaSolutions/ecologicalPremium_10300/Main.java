/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.ecologicalPremium_10300;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	static int[] FarmyardSize, AnimalNumber, EnvironmentFriendliness;

	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		FarmyardSize = new int[N];
		AnimalNumber = new int[N];
		EnvironmentFriendliness = new int[N];
		
		for(int i=0; i<N; i++){
			FarmyardSize[i] = scanner.nextInt();
			AnimalNumber[i] = scanner.nextInt();
			EnvironmentFriendliness[i] = scanner.nextInt();
		}
	}

	static void process(){
		Answer = 0;
		
		for(int i=0; i<N; i++){
			Answer += FarmyardSize[i]*EnvironmentFriendliness[i];
		}
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/