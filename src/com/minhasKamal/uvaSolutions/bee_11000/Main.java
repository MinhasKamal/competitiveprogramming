/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.bee_11000;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static long N;
	
	static long Male, Total;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		if(N<0){
			System.exit(0);
		}
	}
	
	static void process(){
		Male = Total = 0;
		long Female = 0;
		
		if(N>0){
			long[] fibonacii = new long[3];
			Female = fibonacii[0] = 0;
			Male = fibonacii[1] = 1;
			
			for(int i=1; i<N; i++){
				fibonacii[2] = fibonacii[0];
				fibonacii[0] = fibonacii[1];
				fibonacii[1] += fibonacii[2];
				
				Female += fibonacii[0];
				Male += fibonacii[1];
			}
		}
		
		Total = Male+Female+1;
	}
	
	static void output(){
		System.out.println(Male+" "+Total);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/