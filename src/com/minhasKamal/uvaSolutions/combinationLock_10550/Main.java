/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.combinationLock_10550;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int[] Comb;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		Comb = new int[4];
		Comb[0] = scanner.nextInt();
		Comb[1] = scanner.nextInt();
		Comb[2] = scanner.nextInt();
		Comb[3] = scanner.nextInt();
		
		if(Comb[0]==0 && Comb[1]==0 && Comb[2]==0 && Comb[3]==0){
			System.exit(0);
		}
	}
	
	static void process(){
		int turn1=0;
		if(Comb[0]>=Comb[1]){
			turn1=(Comb[0]-Comb[1])*9;
		}else{
			turn1=(Comb[0]+40-Comb[1])*9;
		}
		
		int turn2=0;
		if(Comb[2]>=Comb[1]){
			turn2=(Comb[2]-Comb[1])*9;
		}else{
			turn2=(Comb[2]+40-Comb[1])*9;
		}
		
		int turn3=0;
		if(Comb[2]>=Comb[3]){
			turn3=(Comb[2]-Comb[3])*9;
		}else{
			turn3=(Comb[2]+40-Comb[3])*9;
		}
		Answer=3*360+turn1+turn2+turn3;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/