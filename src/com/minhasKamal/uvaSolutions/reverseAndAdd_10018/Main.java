/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.reverseAndAdd_10018;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static long TestCases;
	static long Case;
	
	static long N;
	
	static long Iteration, Palindrome;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
	}
	
	static void process(){
		Iteration = 0;
		do{
			Iteration ++;
			Palindrome = N+reverse(N);
			N = Palindrome;
		}while(!isPalindrome(Palindrome));
	}
	
	static long reverse(long i){
		long j=i%10;
		i /= 10;
		
		while(i>0){
			j *= 10;
			j += i%10;
			i /= 10;
		}
		
		return j;
	}
	
	static boolean isPalindrome(long i){
		String str = ""+i;
		int fullLength = str.length();
		int halfLength = fullLength/2;
		for(int j=0; j<halfLength; j++){
			if(str.charAt(j) != str.charAt(fullLength-1-j)){
				return false;
			}
		}
		
		return true;
	}
	
	static void output(){
		System.out.println(Iteration+" "+Palindrome);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/