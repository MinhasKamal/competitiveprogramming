/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.texQuotes_272;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String Str1;
	
	static boolean insideQuote = false;
	
	static String Str2;
	
	///***---|||---***///
	
	static void input(){
		Str1 = scanner.nextLine();
	}
	
	static void process(){
		Str2 ="";
		for(int i=0; i<Str1.length(); i++){
			char ch = Str1.charAt(i);
			if(ch=='\"'){
				if(!insideQuote){
					Str2 += "``";
				}else{
					Str2 += "''";
				}
				insideQuote = !insideQuote;
			}else{
				Str2 += ch;
			}
		}
	}
	
	static void output(){
		System.out.println(Str2);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/