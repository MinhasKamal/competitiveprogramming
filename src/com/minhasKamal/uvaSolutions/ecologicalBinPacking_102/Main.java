/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.ecologicalBinPacking_102;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int[] B, G, C;
	
	static String BinColor;
	static int BottleMove;
	
	/***---|||---***/
	
	static void input(){
		B = new int[3];
		G = new int[3];
		C = new int[3];
		for(int i=0; i<3; i++){
			B[i] = scanner.nextInt();
			G[i] = scanner.nextInt();
			C[i] = scanner.nextInt();
        }
	}
	
	static void process(){
		String[] BinColors = new String[]{
			"BCG", "BGC", "CBG", "CGB", "GBC", "GCB"
		};
		int[] bottleMoves = new int[6];
		
		bottleMoves[0] = B[1]+B[2]+C[0]+C[2]+G[0]+G[1]; //BCG
		bottleMoves[1] = B[1]+B[2]+G[0]+G[2]+C[0]+C[1]; //BGC
		bottleMoves[2] = C[1]+C[2]+B[0]+B[2]+G[0]+G[1]; //CBG
		bottleMoves[3] = C[1]+C[2]+G[0]+G[2]+B[0]+B[1]; //CGB
		bottleMoves[4] = G[1]+G[2]+B[0]+B[2]+C[0]+C[1]; //GBC
		bottleMoves[5] = G[1]+G[2]+C[0]+C[2]+B[0]+B[1]; //GCB

		BottleMove = bottleMoves[0];
		BinColor = BinColors[0];
		for(int i=1; i<6; i++){
			if(BottleMove > bottleMoves[i]){
				BottleMove = bottleMoves[i];
				BinColor = BinColors[i];
			}
		}
	}
	
	static void output(){
		System.out.println(BinColor+" "+BottleMove);
	}
	
	/***---|||---***/
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/