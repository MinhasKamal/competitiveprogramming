/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.languageDetection_12250;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int Case;
	static String Word;
	
	static String[] LanguageList = {"UNKNOWN", "ENGLISH", "SPANISH", "GERMAN", "FRENCH", "ITALIAN", "RUSSIAN"};
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		Word = scanner.next();
		if(Word.equals("#")){
			System.exit(0);
		}
	}
	
	static void process(){
		Answer=0;
		
		if(Word.equals("HELLO")){
			Answer=1;
		}else if(Word.equals("HOLA")){
			Answer=2;
		}else if(Word.equals("HALLO")){
			Answer=3;
		}else if(Word.equals("BONJOUR")){
			Answer=4;
		}else if(Word.equals("CIAO")){
			Answer=5;
		}else if(Word.equals("ZDRAVSTVUJTE")){
			Answer=6;
		}
		
		Case++;
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+LanguageList[Answer]);
		
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		Case=0;
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/