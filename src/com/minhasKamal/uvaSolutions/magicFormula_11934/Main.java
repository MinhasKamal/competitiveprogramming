/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.magicFormula_11934;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int A, B, C, D, L;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		A = scanner.nextInt();
		B = scanner.nextInt();
		C = scanner.nextInt();
		D = scanner.nextInt();
		L = scanner.nextInt();
		
		if(D==0){
			System.exit(0);
		}
	}
	
	static void process(){
		Answer=0;
		
		int y;
		for(int i=0; i<=L; i++){
			y = A*i*i + B*i + C;
			if(y%D == 0){
				Answer++;
			}
		}
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/