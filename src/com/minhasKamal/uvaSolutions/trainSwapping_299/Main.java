/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.trainSwapping_299;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	static int[] Nums;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		
		Nums = new int[N];
		for(int i=0; i<N; i++){
			Nums[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		Answer = 0;
		
		int temp;
		for(int i=0; i<N; i++){
			for(int j=i; j<N; j++){
				if(Nums[i] > Nums[j]){
					temp = Nums[i];
					Nums[i] = Nums[j];
					Nums[j] = temp;
					
					Answer++;
				}
			}
		}
	}
	
	static void output(){
		System.out.println("Optimal train swapping takes "+Answer+" swaps.");
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/