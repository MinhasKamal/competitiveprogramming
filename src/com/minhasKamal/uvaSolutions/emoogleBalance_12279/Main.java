/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.emoogleBalance_12279;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int Case;
	
	static int N;
	static int[] Events;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		if(N==0){
			System.exit(0);
		}
		
		Events = new int[N];
		for(int i=0; i<N; i++){
			Events[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		
		int reasons=0, treats=0;
		for(int i=0; i<N; i++){
			if(Events[i]>0){
				reasons++;
			}else{
				treats++;
			}
		}
		
		Answer=reasons-treats;
		
		Case++;
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		Case=0;
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/