/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.orderingTasks_10305;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N, M;
	static int[][] Adjacency;
	static int[] dependency;
	
	static int[] Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		M = scanner.nextInt();
		if(N==0){
			System.exit(0);
		}
		
		Adjacency = new int[M][2];
		for(int i=0; i<M; i++){
			Adjacency[i][0] = scanner.nextInt();
			Adjacency[i][1] = scanner.nextInt();
		}
	}
	
	static void process(){
		Answer = new int[N];
		dependency = calculateDependency();
		
		int indexPrev=0, indexNew = 0;
		while(indexNew < N){
			for(int i=0; i<N; i++){
				if(dependency[i]==0){
					Answer[indexNew] = i+1;
					indexNew++;
					dependency[i]--;
				}
			}
			
			for(int i=indexPrev; i<indexNew; i++){
				removeDependency(Answer[i]);
			}
			
			indexPrev=indexNew;
		}
	}
	
	static void removeDependency(int node){
		for(int i=0; i<M; i++){
			if(Adjacency[i][0]==node){
				dependency[Adjacency[i][1]-1]--;
			}
		}
	}
	
	static int[] calculateDependency(){
		int[] dependency = new int[N];
		
		for(int i=0; i<M; i++){
			dependency[Adjacency[i][1]-1]++;
		}
		
		return dependency;
	}
	
	static void output(){
		for(int i=0; i<N; i++){
			System.out.print(Answer[i]+" ");
		}
		System.out.println();
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/