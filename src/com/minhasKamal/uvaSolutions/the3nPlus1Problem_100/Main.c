/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

#include <stdio.h>

int cycle(int i, int j){
    if(i>j){
        i^=j^=i^=j;
    }

    int m=1;
    int n, k;
    for( ; i<=j; i++){
        n=i;
        for(k=1; n>1; k++){
            if(n%2==0) n/=2;
            else n=(3*n+1)/2, k++;
        }
        if(k>m) m=k;
    }

    return m;
}

int main(){
    int i, j;
    while(scanf("%d %d", &i, &j) != EOF){
        if(i<=0) break;
        printf("%d %d %d\n", i, j, cycle(i, j));
    }
    return 0;
}
