/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.the3nPlus1Problem_100;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N1, N2;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		if(!scanner.hasNext()){
			System.exit(0);
		}
		
		N1 = scanner.nextInt();
		N2 = scanner.nextInt();
	}
	
	static void process(){
		int min=N1, max=N2;
		if(N1>N2){
			min=N2;
			max=N1;
		}
		
		Answer=0;
		for(int i=min; i<=max; i++){
			int cycleLength = algorithm(i);
			if(Answer<cycleLength){
				Answer = cycleLength;
			}
		}
	}
	
	static int algorithm(int n){
		if(n==1){
			return 1;
		}
		
		if(n%2!=0){
			n = 3*n+1;
		}else{
			n = n/2;
		}
		
		return 1+algorithm(n);
	}
	
	static void output(){
		System.out.println(N1+" "+N2+" "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(true){
			input();
			process();
			output();
		}
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/