/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.lumberjackSequencing_11942;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int[] N;
	
	static boolean Answer;
	
	///***---|||---***///
	
	static void input(){
		N = new int[10];
		for(int i=0; i<10; i++){
			N[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		Answer = true;
		
		if(N[0]>N[1]){
			for(int i=2 ; i<10; i++){
				if(N[i-1]<N[i]){
					Answer = false;
					break;
				}
			}
		}else{
			for(int i=2 ; i<10; i++){
				if(N[i-1]>N[i]){
					Answer = false;
					break;
				}
			}
		}
	}
	
	static void output(){
		if(Answer){
			System.out.println("Ordered");
		}else{
			System.out.println("Unordered");
		}
		
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		System.out.println("Lumberjacks:");
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/