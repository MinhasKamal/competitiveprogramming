/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.egypt_11854;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int A, B, C;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		A = scanner.nextInt();
		B = scanner.nextInt();
		C = scanner.nextInt();
		if(A==0){
			System.exit(0);
		}
		
		if(C<A){
			int t = C;
			C = A;
			A = t;
		}
		
		if(C<B){
			int t = C;
			C = B;
			B = t;
		}
	}
	
	static void process(){
		if(A*A+B*B == C*C){
			Answer="right";
		}else{
			Answer="wrong";
		}
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/