/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.youCanSay11_10929;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String N;
	
	static boolean Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextLine();
		if(N.charAt(0) == 48 && N.length()==1){
			System.exit(0);
		}
	}
	
	static void process(){
		Answer=true;
		
		int sum=0;
		for(int i=0; i<N.length(); i++){
			if(i%2==0){
				sum += N.charAt(i)-48;
			}else{
				sum -= N.charAt(i)-48;
			}
		}
		
		if(sum%11 != 0){
			Answer=false;
		}
	}
	
	static void output(){
		if(Answer){
			System.out.println(N+" is a multiple of 11.");
		}else{
			System.out.println(N+" is not a multiple of 11.");
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/