/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.automaticAnswer_11547;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
	}
	
	static void process(){
		Answer = (( ( (N*63)+7492 )*5 )-498) %100;
		Answer /= 10;
		if(Answer<0){
			Answer = -Answer;
		}
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/