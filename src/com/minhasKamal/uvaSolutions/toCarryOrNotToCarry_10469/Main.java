/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.toCarryOrNotToCarry_10469;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int A, B;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		A = scanner.nextInt();
		B = scanner.nextInt();
	}
	
	static void process(){
		Answer=A^B;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/