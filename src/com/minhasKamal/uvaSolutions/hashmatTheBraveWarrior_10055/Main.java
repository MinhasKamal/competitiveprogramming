/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.hashmatTheBraveWarrior_10055;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static long N1, N2;
	
	static long Answer;
	
	///***---|||---***///
	
	static void input(){
		N1 = scanner.nextLong();
		N2 = scanner.nextLong();
	}
	
	static void process(){
		Answer = N2-N1;
		
		if(Answer<0){
			Answer = -Answer;
		}
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/