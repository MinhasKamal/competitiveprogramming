/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.oddSum_10783;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N1, N2;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N1 = scanner.nextInt();
		N2 = scanner.nextInt();
	}
	
	static void process(){
		if(N1>N2){
			int t = N1;
			N1 = N2;
			N2 = t;
		}
		
		if(N1%2 == 0){
			N1++;
		}
		if(N2%2 != 0){
			N2++;
		}
		
		Answer=0;
		for(int i=N1; i<N2; i+=2){
			Answer += i;
		}
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/