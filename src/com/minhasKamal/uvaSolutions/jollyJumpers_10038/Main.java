/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.jollyJumpers_10038;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N;
	static int[] Nums;
	
	static boolean Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		Nums = new int[N];
		for(int i=0; i<N; i++){
			Nums[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		Answer=true;
		
		if(N>1){
			int[] diffs = new int[N-1];
			
			for(int i=0; i<N-1; i++){
				diffs[i] = abs(Nums[i]-Nums[i+1]);
			}
			
			diffs = sort(diffs);
			
			for(int i=0; i<N-1; i++){
				if(diffs[i] != i+1){
					Answer = false;
				return;
				}
			}
		}
	}
	
	static int[] sort(int nums[]){
		int temp;
		for(int i=0; i<nums.length; i++){
			for(int j=i; j<nums.length; j++){
				if(nums[i]>nums[j]){
					temp = nums[i];
					nums[i] = nums[j];
					nums[j] = temp;
				}
			}
		}
		
		return nums;
	}
	
	static int abs(int i){
		if(i<0){
			return -i;
		}
		return i;
	}
	
	static void output(){
		if(Answer){
			System.out.println("Jolly");
		}else{
			System.out.println("Not jolly");
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/