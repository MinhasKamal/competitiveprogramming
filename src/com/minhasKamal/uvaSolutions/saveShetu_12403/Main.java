/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.saveShetu_12403;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static String Operation;
	static int Amount;
	static boolean Report;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		
		Operation = scanner.next();
		if(Operation.equals("donate")){
			Report = false;
			Amount = scanner.nextInt();
		}else{
			Report = true;
		}
	}
	
	static void process(){
		if(!Report){
			Answer += Amount;
		}
	}
	
	static void output(){
		if(Report){
			System.out.println(Answer);
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/