/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.ananagrams_156;/**/

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static ArrayList<String> Dictionary = new ArrayList<String>();
	
	///***---|||---***///
	
	static void input(){
		while(scanner.hasNext()){
			String StrTemp = scanner.next();
			if(StrTemp.equals("#")){
				break;
			}
			Dictionary.add(StrTemp);
		}
	}
	
	static void process(){
		boolean hasAnagram;
		for(int i=0; i<Dictionary.size(); i++){
			hasAnagram = false;
			for(int j=i+1; j<Dictionary.size(); j++){
				if(isAnagram(Dictionary.get(i), Dictionary.get(j))){
					hasAnagram = true;
					Dictionary.remove(j);
					j--;
				}
			}
			if(hasAnagram){
				Dictionary.remove(i);
				i--;
			}
		}
		
		sort();
	}
	
	static boolean isAnagram(String str1, String str2){
		if(str1.length()!=str2.length()){
			return false;
		}
		
		str1 = str1.toLowerCase();
		str2 = str2.toLowerCase();
		for(int i=0; i<str1.length(); i++){
			int pos = str2.indexOf(str1.charAt(i));
			
			if(pos<0){
				return false;
			}
			str2 = str2.substring(0,pos)+'0'+str2.substring(pos+1);
		}
		
		
		return true;
	}
	
	static void sort(){
		for(int i=0; i<Dictionary.size(); i++){
			for(int j=i+1; j<Dictionary.size(); j++){
				if(compare(Dictionary.get(i), Dictionary.get(j))){
					String strTemp = Dictionary.get(i);
					Dictionary.set(i, Dictionary.get(j));
					Dictionary.set(j, strTemp);
				}
			}
		}
	}
	
	static boolean compare(String str1, String str2){
		for(int i=0; i<str1.length(); i++){
			if(i>=str2.length()){
				return true;
			}
			if(str1.charAt(i)>str2.charAt(i)){
				return true;
			}if(str1.charAt(i)<str2.charAt(i)){
				return false;
			}
		}
		
		return false;
	}
	
	static void output(){
		for(int i=0; i<Dictionary.size(); i++){
			System.out.println(Dictionary.get(i));
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);

		input();
		process();
		output();
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/