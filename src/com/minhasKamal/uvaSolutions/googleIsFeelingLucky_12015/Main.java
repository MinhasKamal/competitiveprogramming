/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.googleIsFeelingLucky_12015;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static String[] Website;
	static int[] Relevance;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		Website = new String[10];
		Relevance = new int[10];
		
		for(int i=0; i<10; i++){
			Website[i] = scanner.next();
			Relevance[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		Answer = 0;
		for(int i=0; i<10; i++){
			if(Answer < Relevance[i]){
				Answer = Relevance[i];
			}
		}
	}
	
	static void output(){
		System.out.println("Case #"+Case+":");
		for(int i=0; i<10; i++){
			if(Answer == Relevance[i]){
				System.out.println(Website[i]);
			}
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/