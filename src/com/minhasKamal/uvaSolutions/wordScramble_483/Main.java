/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.wordScramble_483;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String IN;
	
	static String Out;
	
	///***---|||---***///
	
	static void input(){
		IN = scanner.nextLine();
	}
	
	static void process(){
		
		String[] words = IN.split(" ");
		Out=""+makeRev(words[0]);
		
		for(int i=1; i<words.length; i++){
			String revWord = makeRev(words[i]);
			Out += " "+revWord;
		}
	}
	
	static String  makeRev(String word){
		String revWord = "";
		
		for(int i=word.length()-1; i>=0; i--){
			revWord += word.charAt(i);
		}
		
		return revWord;
	}
	
	static void output(){
		System.out.println(Out);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/