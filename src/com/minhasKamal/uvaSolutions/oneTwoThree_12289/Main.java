/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.oneTwoThree_12289;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static String N;
	static String[] Nums = {"one", "two", "three"};
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.next();
	}
	
	static void process(){
		Answer = getNum();
	}
	
	static int getNum(){
		
		int value=0;
		for(int i=0; i<Nums.length; i++){
			if(isPartialMatch(N, Nums[i], 1)){
				value = i+1;
				break;
			}
		}
		
		return value;
	}
	
	static boolean isPartialMatch(String str1, String str2, int tolerance){
		
		int length = str1.length();
		if(length != str2.length()){
			return false;
		}
		
		boolean flag = true;
		int misMatch = 0;
		for(int i=0; i<length; i++){
			if(str1.charAt(i) != str2.charAt(i)){
				misMatch++;
				if(misMatch>tolerance){
					flag = false;
					break;
				}
			}
		}
		
		return flag;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/