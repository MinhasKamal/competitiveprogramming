/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.costCutting_11727;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int A, B, C;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		A = scanner.nextInt();
		B = scanner.nextInt();
		C = scanner.nextInt();
	}
	
	static void process(){
		if((A>B && A<C) || (A<B && A>C)){
			Answer = A;
		}else if((B>A && B<C) || (B<A && B>C)){
			Answer = B;
		}else{
			Answer = C;
		}
	}
	
	static void output(){
		System.out.println("Case " + Case + ": " + Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/
