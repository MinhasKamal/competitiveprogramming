/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.kindergartenCountingGame_494;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String Str;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		Str = scanner.nextLine();
	}
	
	static void process(){
		Answer=0;
		
		boolean insideWord = false;
		for(int i=0; i<Str.length(); i++){
			int ch = (int) Str.charAt(i);
			if(isAlph(ch)){
				if(!insideWord){
					insideWord = true;
					Answer++;
				}
			}else{
				insideWord = false;
			}
		}
		
	}
	
	static boolean isAlph(int ch){
		if((ch>64 && ch<91) || (ch>96 && ch<123)){
			return true;
		}
		
		return false;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/