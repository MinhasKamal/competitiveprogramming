/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.summingDigits_11332;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		if(N==0){
			System.exit(0);
		}
	}
	
	static void process(){
		Answer = N;
		while(Answer > 9){
			Answer = addDigits(Answer);
		}
	}
	
	static int addDigits(int num){
		int sum=0;
		
		while(num>0){
			sum += num%10;
			num /= 10;
		}
		
		return sum;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/