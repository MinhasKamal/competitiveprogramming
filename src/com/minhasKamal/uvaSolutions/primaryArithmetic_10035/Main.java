/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.primaryArithmetic_10035;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static long N1, N2;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N1 = scanner.nextLong();
		N2 = scanner.nextLong();
		if(N1==0 && N2==0){
			System.exit(0);
		}
	}
	
	static void process(){
		
		Answer=0;
		
		long temp;
		if(N1<N2){
			temp = N1;
			N1 = N2;
			N2 = temp;
		}
		
		long sum = 0;
		while(N1>0){
			sum = (N1%10) + (N2%10) + sum;
			
			N1 /= 10;
			N2 /= 10;
			sum /= 10;
			
			if(sum>0){
				Answer++;
			}
		}
	}
	
	static void output(){
		if(Answer==0){
			System.out.println("No carry operation.");
		}else if(Answer==1){
			System.out.println("1 carry operation.");
		}else{
			System.out.println(Answer+" carry operations.");
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/