/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.littleNephew_12463;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int H, T, P, So, S;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		H = scanner.nextInt();
		T = scanner.nextInt();
		P = scanner.nextInt();
		So = scanner.nextInt();
		S = scanner.nextInt();
		if(H==0){
			System.exit(0);
		}
	}
	
	static void process(){
		
		Answer = H*T*P*So*So*S*S;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/