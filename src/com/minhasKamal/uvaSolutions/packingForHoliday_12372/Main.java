/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.packingForHoliday_12372;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int Len = 20;
	static int L, W, H;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		L = scanner.nextInt();
		W = scanner.nextInt();
		H = scanner.nextInt();
	}
	
	static void process(){
		if(isFit()){
			Answer = "good";
		}else{
			Answer = "bad";
		}
	}
	
	static boolean isFit(){
		
		if(L > Len){
			return false;
		}if(W > Len){
			return false;
		}if(H > Len){
			return false;
		}
		
		return true;
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/