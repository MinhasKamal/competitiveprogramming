/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.eventPlanning_11559;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N, B, H, W;
	static int P;
	static boolean isAvailable;
	
	static int MinPrice;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		B = scanner.nextInt();
		H = scanner.nextInt();
		W = scanner.nextInt();
	}
	
	static void process(){
		Answer = -1;
		
		for(int i=0; i<H; i++){
			P = scanner.nextInt();
			isAvailable = false;
			
			for(int j=0; j<W; j++){
				if(N <= scanner.nextInt()){
					isAvailable = true;
				}
			}
			
			if(B >= N*P && isAvailable){
				Answer = B = N*P;
			}
		}
	}
	
	static void output(){
		if(Answer>=0){
			System.out.println(Answer);
		}else{
			System.out.println("stay home");
		}
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/