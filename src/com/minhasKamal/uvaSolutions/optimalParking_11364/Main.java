/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.optimalParking_11364;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int N;
	static int[] positions;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		N = scanner.nextInt();
		positions = new int[N];
		for(int i=0; i<N; i++){
			positions[i] = scanner.nextInt();
		}
	}
	
	static void process(){
		int minDistance=positions[0];
		int maxDistance=positions[0];
		for(int i=1; i<N; i++){
			if(minDistance>positions[i]){
				minDistance = positions[i];
			}
			if(maxDistance<positions[i]){
				maxDistance = positions[i];
			}
		}
		Answer = (maxDistance-minDistance)*2;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/