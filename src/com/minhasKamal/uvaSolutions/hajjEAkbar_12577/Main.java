/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.hajjEAkbar_12577;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int Case=0;
	
	static String In;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		In = scanner.next();
	}
	
	static void process(){
		if(In.equals("*")){
			System.exit(0);
		}
		Answer = "Hajj-e-Akbar";
		if(In.length()>4){
			Answer = "Hajj-e-Asghar";
		}
		Case++;
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/
