/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.uglyNumbers_136;/**/

public class Main {
	static int N;
	
	static long Answer;
	
	///***---|||---***///
	
	static void input(){
		N = 1500;
	}
	
	static void process(){
		Answer = 859963392L;
		if(Answer>0){
			return;
		}
		
		int uglyNumber = 0, count = 1;
		for(int i=1; count<=N; i++){
			uglyNumber = i;
			
			while(uglyNumber%5==0){
				uglyNumber /= 5;
			}
			while(uglyNumber%3==0){
				uglyNumber /= 3;
			}
			while(uglyNumber%2==0){
				uglyNumber /= 2;
			}
			
			if(uglyNumber==1){
				Answer = i;
				count++;
			}
		}
	}
	
	static void output(){
		System.out.println("The 1500'th ugly number is "+Answer+".");
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		input();
		process();
		output();
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/