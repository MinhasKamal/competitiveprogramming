/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.whatIsTheFrequencyKenneth_499;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static String Str;
	
	static int[] CharFreq;
	static int MaxFreq;
	
	///***---|||---***///
	
	static void input(){
		Str = scanner.nextLine();
	}
	
	static void process(){
		CharFreq = new int[126];
		MaxFreq=0;
		
		for(int i=0; i<Str.length(); i++){
			int ch = (int) Str.charAt(i);
			
			if(ch>60){
				CharFreq[ch]++;
				if(MaxFreq<CharFreq[ch]){
					MaxFreq=CharFreq[ch];
				}
			}
		}
	}
	
	static void output(){
		for(int i=60; i<126; i++){
			if(MaxFreq==CharFreq[i]){
				System.out.print((char) i);
			}
		}
		System.out.println(" "+MaxFreq);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/