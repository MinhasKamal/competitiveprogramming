/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.automateTheGrades_11777;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int TestCases;
	static int Case;
	
	static int Term1, Term2, Final, Attendance, Class_Test1, Class_Test2, Class_Test3;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		Term1 = scanner.nextInt();
		Term2 = scanner.nextInt();
		Final = scanner.nextInt();
		Attendance = scanner.nextInt();
		Class_Test1 = scanner.nextInt();
		Class_Test2 = scanner.nextInt();
		Class_Test3 = scanner.nextInt();
	}
	
	static void process(){
		int Min_Class_Test = 0;
		if(Class_Test1<Class_Test2){
			Min_Class_Test = Class_Test1;
		}else{
			Min_Class_Test = Class_Test2;
		}
		if(Class_Test3<Min_Class_Test){
			Min_Class_Test = Class_Test3;
		}
		
		int Class_Test = (Class_Test1+Class_Test2+Class_Test3-Min_Class_Test)/2;
		int Marks = Term1 + Term2 + Final + Attendance + Class_Test;

		if(Marks >= 90){
			Answer = "A";
		}else if(Marks >= 80){
			Answer = "B";
		}else if(Marks >= 70){
			Answer = "C";
		}else if(Marks >= 60){
			Answer = "D";
		}else{
			Answer = "F";
		}
	}
	
	static void output(){
		System.out.println("Case "+Case+": "+Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		TestCases = scanner.nextInt();
		for(Case=1; Case<=TestCases; Case++){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/