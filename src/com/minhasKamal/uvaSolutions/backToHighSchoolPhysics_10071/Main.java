/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.backToHighSchoolPhysics_10071;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int V, T;
	
	static int Answer;
	
	///***---|||---***///
	
	static void input(){
		V = scanner.nextInt();
		T = scanner.nextInt();
	}
	
	static void process(){
		Answer = 2*V*T;
	}
	
	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNext()){
			input();
			process();
			output();
		}
		
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/