/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
***********************************************************/

/**/package com.minhasKamal.uvaSolutions.divisionOfNlogonia_11498;/**/

import java.util.Scanner;

public class Main {
	static Scanner scanner;
	
	static int N;
	static int X, Y;
	static int x, y;
	
	static String Answer;
	
	///***---|||---***///
	
	static void input(){
		x = scanner.nextInt(); 
		y = scanner.nextInt();
	}
	
	static void process(){
		if(x>X && y>Y){
			Answer = "NE";
		}else if(x<X && y>Y){
			Answer = "NO";
		}else if(x<X && y<Y){
			Answer = "SO";
		}else if(x>X && y<Y){
			Answer = "SE";
		}else{
			Answer = "divisa";
		}
	}

	static void output(){
		System.out.println(Answer);
	}
	
	///***---|||---***///
	
	public static void main(String[] args) throws Exception{
		/**/System.setIn(new java.io.FileInputStream(Main.class.getResource("input.txt").getPath()));/**/
		scanner = new Scanner(System.in);
		
		while(scanner.hasNextLine()){
			N = scanner.nextInt();
			if(N==0){
				break;
			}
			
			X = scanner.nextInt();
			Y = scanner.nextInt();
			
			for(int i=0; i<N; i++){
				input();
				process();
				output();
			}
		}
	}
}

/**
 * DO NOT FORGET BEFORE SUBMITTING
 * 1. comment package name
 * 2. comment system input
**/
